Dimensions
LOA: 49 ft 2 in
Beam: 16 ft 5 in
Maximum Draft: 3 ft 1 in
Dry Weight: 20000 kgs

Engine Options
Engine Option 1: 2x 550 HP
 
Tanks
Fresh Water Tanks: (100 Gallons)
Fuel Tanks: (400 Gallons)

Accommodations
Number of cabins: 3
Number of heads: 2